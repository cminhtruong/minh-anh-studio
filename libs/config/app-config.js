app.config(function ($routeProvider, $locationProvider) {
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
    $routeProvider
        .when("/", {
            templateUrl: "libs/views/main.html",
            controller: 'mainController'
        })
        .when("/about", {
            templateUrl: "libs/views/about.html",
            controller: 'aboutController'
        })
        .when("/contact", {
            templateUrl: "libs/views/contact.html",
            controller: 'contactController'
        });
});
